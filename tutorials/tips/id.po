msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tips and Tricks\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2010-10-11 10:15+0700\n"
"Last-Translator: @CameliaGirls Translation Team <cameliagirls@googlegroups."
"com>\n"
"Language-Team: Indonesian <id@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "pengakuan untuk penerjemah < >, "

#. (itstool) path: articleinfo/title
#: tutorial-tips.xml:6
msgid "Tips and Tricks"
msgstr "Tips dan Trik"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tips.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tips.xml:11
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"Tutorial ini akan mendemonstrasikan bermacam-macam tips dan trik yang sudah "
"dipelajari oleh pengguna dalam menggunakan Inkscape dan beberapa fitur "
"“tersembunyi“ yang bisa membantu mempercepat pekerjaan."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:17
#, fuzzy
msgid "Radial placement with Tiled Clones"
msgstr "Penempatan radial dengan Tile Clones"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:18
#, fuzzy
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"Sangat mudah untuk melihat bagaimana menggunakan dialog <command>Tile "
"Clones</command> untuk grid rectangular dan pola-pola. Tetapi bagaimana jika "
"anda membutuhkan penempatan <firstterm>radial</firstterm>, dimana obyek "
"saling berbagi titik putar tengah yang sama? Tentu saja bisa!"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:23
#, fuzzy
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"Jika pola radial hanya membutuhkan 3, 4, 6, 8, atau 12 elemen, anda bisa "
"mencoba simetri P3, P31M, P3M1, P4, P4M, P6, atau P6M. Ini akan menghasilkan "
"bentuk seperti salju dan sejenisnya. Meskipun begitu, terdapat metode umum, "
"yaitu,"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:28
#, fuzzy
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"Pilihlah simetri P1 (translasi sederhana - simple translation) kemudian "
"<emphasis>kompensasikan<emphasis> translasi tersebut dengan tab "
"<command>Shift</command> dan aturlah <command>Per row/Shift Y</command> dan "
"<command>Per coloumn/Shift X</command> masing-masing -100%. Kini semua klon "
"akan disusun tepat diatas asalnya. Yang tersisa hanyalah merotasinya dengan "
"tab <command>Rotation</command> dan mengatur sudut rotasi per kolom, "
"kemudian membuat polanya dengan satu baris dan banyak kolom. Sebagai contoh, "
"berikut adalah pola yang dihasilkan dengan garis horisontal, dengan 30 "
"kolom, masing-masing diputar 6 derajat:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:43
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"Untuk mendapatkan clock dial - bentuk muka jam, yang anda butuhkan hanyal "
"memotong atau meng-overlay bagian tengah dari lingkaran putih (untuk "
"melakukan operasi boolean pada klon, unlinklah terlebih dahulu)."

#. (itstool) path: sect1/para
#: tutorial-tips.xml:47
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"Efek yang lebih menarik bisa dihasilkan dengan menggunakan baris dan kolom. "
"Berikut adalah sebuah pola dengan 10 kolom dan 8 baris, dengan rotasi "
"sebesar 2 derajat per baris dan 18 per kolom. Masing-masing grup garis "
"adalah ”kolom”, jadi, grup-grup tersebut masing-masing berjarak 18 derajat, "
"garis individualnya terpisah 2 derajat:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:59
#, fuzzy
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"Pada contoh diatas, garisnya dirotasi mengitari titik tengahnya. Tetapi "
"bagaimana jika anda ingin tengahnya adalah bagian luar dari shape, bentuk "
"yang anda inginkan? Buathal sebuah rectangle transparan (tanpa isi, tanpa "
"garis pinggir) yang menutupi bentuk yang ingin anda ubah dan titik tengah "
"dari rectangle tersebut diluar bentuk tersebut, grup keduanya, kemudian "
"gunakan <command>Tile Clones</command>. Inilah cara anda membuat ”ledakan” "
"atau ”kembang api” dengan skala random, rotasi, dan opasitas yang "
"memungkinkan:"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:76
#, fuzzy
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr "Bagaimana memotong (area ekspor banyak rectangular)?"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:77
#, fuzzy
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<menuchoice><guimenu>File</guimenu><guimenuitem>Export PNG Image</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Then the rects will remember their export filenames. After "
"that, it's very easy to re-export some of the rects: switch to the export "
"layer, use <keycap>Tab</keycap> to select the one you need (or use Find by "
"id), and click <guibutton>Export</guibutton> in the dialog. Or, you can "
"write a shell script or batch file to export all of your areas, with a "
"command like:"
msgstr ""
"Buatlah sebuah layer baru, didalamnya terdapat rectangle transparan yang "
"menutupi bagian dari gambar anda. Pastikan dokumen menggunakan satuan px "
"(bawaan), nyalakan grid dan jepit rectangularnya pada grid sehingga masing-"
"masing sesuai dengan satuan px. Berikan id yang bisa dipahami pada masing-"
"masing rectangular, kemudian ekspor satu-satu menjadi filenya masing-masing. "
"Maka, setiap rectangular akan mengingat nama berkasnya jika diekspor. "
"Setelah itu, sangatlah mudah untuk mere-ekspor beberapa dari rectangular "
"tersebut: berpindahlah ke layer export, gunakan Tab untuk memilih (atau "
"gunakan Find by id), dan klik Export pada dialog. Atau anda bisa saja "
"membuat sebuah skrip shell atau berkas bat untuk mengekspor seluruh area, "
"dengan perintah misalnya:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:88
#, fuzzy
msgid "<command>inkscape -i area-id -t filename.svg</command>"
msgstr "inkscape -i area-id -t namaberkas.svg"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:91
#, fuzzy
msgid ""
"for each exported area. The <command>-t</command> switch tells it to use the "
"remembered filename hint, otherwise you can provide the export filename with "
"the <command>-e</command> switch. Alternatively, you can use the "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
"results."
msgstr ""
"untuk setiap area yang diekspor. -t adalah opsi untuk mengingat nama berkas, "
"jika tidak, anda bisa mendapatkan nama berkasi ekspor dengan -e. Alternatif, "
"anda bisa menggunakan perangkat <ulink url=\"http://www.digitalunleashed.com/"
"\">svgslice</ulink> yang akan otomatis mengekspor dari dokumen SVG Inkscape, "
"berpatokan pada slice layer atau guides."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:101
msgid "Non-linear gradients"
msgstr "Gradien non-linear"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:102
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"SVG versi 1.1 tidak mendukung gradien non-linear (mis. yang memiliki "
"translasi non-linear antar warna). Pun begitu, anda bisa mengemulasikannya "
"dengan gradien <firstterm>multistop</firstterm>."

#. (itstool) path: sect1/para
#: tutorial-tips.xml:106
#, fuzzy
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"Mulailah dengan dua-titik gradien yang sederhana. Bukalah editor Gradient "
"(klik ganda pada handle gradien pada Gradient tooL). Tambahkan gradient stop "
"baru ditengah; seret sedikit. Kemudian tambahkan lagi stop sebelum dan "
"sesudah stop tengah tersebut kemudian geret lagi, sehingga gradien yang "
"dihasilkan halus. Semakin banyak stop yang anda tambahkan, semakin halus "
"hasilnya. Berikut adalah gradien hitam-putih dengan dua stop:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:121
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"Dan berikut adalah macam-macam gradien multi-stop “non-linear” (periksalah "
"dengan Gradient Editor):"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:133
msgid "Excentric radial gradients"
msgstr "Gradien radial eksentrik"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:134
#, fuzzy
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap function=\"shift"
"\">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</"
"firstterm> of the gradient away from its center. When you don't need it, you "
"can snap the focus back by dragging it close to the center."
msgstr ""
"Gradien radial tidaklah harus simetrik. Pada Gradient tool, seret handle "
"tenga dari gradien eliptik dengan <keycap>Shift</keycap>. Ini akan "
"memindahkan <firstterm>focus handle</firstterm> bertanda x dari gradien "
"menjauh dari titik tengahnya. Saat anda tidak membutuhkannya, anda bisa "
"menjepit fokusnya kembali dengan menyeretnya ke tengah."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:149
msgid "Aligning to the center of the page"
msgstr "Meratakan ke tengah halaman"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:150
#, fuzzy
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"Untuk meratakan sesuatu ke tengah halaman, pilih obyeknya atau jadikan grup "
"kemudian pilih <command>Page</command> dari daftar <command>Relative to:</"
"command> pada dialog Allign (<keycap>Ctrl+Shift+A</keycap>)."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:158
msgid "Cleaning up the document"
msgstr "Membersihkan dokumen"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:159
#, fuzzy
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"Banyak dari gradien, pola, penanda, yang tidak terpakai (terutama yang anda "
"edit secara manual) tetap pada pallet dalam dokumen dan bisa digunakan untuk "
"obyek baru. Tetapi, jika anda ingin mengoptimalisasi dokumen anda, gunakan "
"perintah <command>Vacuum Defs</command> pada menu File. Ini akan membuang "
"semua gradien, pola, atau penanda yang tidak digunakan oleh dokumen, dan "
"membuat ukuran berkas lebih kecil."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:168
msgid "Hidden features and the XML editor"
msgstr "Fitur tersembunyi dari editor XML"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:169
#, fuzzy
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"XML editor membuat anda bisa merubah hampir semua aspek dari dokumen tanpa "
"menggunakan edior teks eksternal. Inkscape juga mendukung lebih banyak fitur "
"SVG yang tidak bisa diakses lewat GUI. XML editor adalah salah satu cara "
"untuk mengakses fitur-fitur tersebut (jika anda memahami SVG)."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:178
msgid "Changing the rulers' unit of measure"
msgstr "Merubah takaran satuan ukur"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:179
#, fuzzy
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-right corner "
"and preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Page</"
"guimenuitem> tab."
msgstr ""
"Pada template bawaan, satuan ukuran yang digunakan adalah px (“SVG user "
"unit”, pada Inkscape sama dengan 0.8pt atau 1/90 inch). Ini juga merupakan "
"satuan yang digunakan dalam menampilkan koordinat pada sudit kiri bawah dan "
"bawaan dari unit pada semua menu. (Anda juga bisa mencoba menyorot dengan "
"tetikus pada penggaris untuk memunculkan tooltip yang menampilkan satuan "
"yang digunakan.) Untuk merubahnya, bukalah <command>Document Preferences</"
"comman> (<keycap>Ctrl+Shift+D</keycap>) dan ubahlah <command>Default units</"
"command> pada tab <command>page</command>."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:189
msgid "Stamping"
msgstr "Cap"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:190
#, fuzzy
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"Untuk membuat banyak duplikat sebuah obyek secara cepat, gunakanlah fitur "
"<firstterm>stamping<firstterm>-cap. Seret sebuah obyek (atau skala ulang "
"atau rotasilah), dan sambil tetap menekan tombol tetikus, tekan "
"<keycap>Spasi</keycap> pada keyboard. Ini akan meninggalkan ”cap” pada "
"bentuk dan lokasi obyek saat itu. Anda bisa mengulangnya sebanyak yang anda "
"inginkan."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:198
msgid "Pen tool tricks"
msgstr "Trik pen tool"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:199
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"Pada tool Pen (Bezier), anda memiliki pilihan-pilihan berikut dalam "
"menyelesaikan sebuah garis:"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:204
msgid "Press <keycap>Enter</keycap>"
msgstr "Menekan <keycap>Enter</keycap>"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:209
#, fuzzy
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr "Klik ganda dengan tombol kiri tetikus"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:214
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:219
msgid "Select another tool"
msgstr "Memilih tool yang lain"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:224
#, fuzzy
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"Ingatlah jika path belum selesai (ditampilkan dalam hijau, dengan segmen "
"merah) ia tidak akan eksis sebagai obyek pada dokumen. Untuk itu, jika anda "
"ingin membatalkan, gunakan <keycap>Esc</keycap> (membatalkan seluruh path) "
"atau <keycap>Backspace</keycap> (menghilangkan segmen terakhir yang belum "
"selesai) ketimbang menggunakan <command>Undo</keycap>."

#. (itstool) path: sect1/para
#: tutorial-tips.xml:230
#, fuzzy
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"Untuk menambahkan subpath baru pada path yang sudah ada, pilihlah path "
"tersebut dan mulailah menggambar dengan <keycap>Shift</keycap> dari titik "
"arbitarynya. Jika apa yang anda inginkan hanyalah <emphasis>meneruskan</"
"emphasis> sebuah path yang ada, Shift tidak diperlukan; Langsung mulai saja "
"menggambar dari salah satu anchor pada ujungnya."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:238
msgid "Entering Unicode values"
msgstr "Memasukkan nilai Unicode"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:239
#, fuzzy
msgid ""
"While in the Text tool, pressing <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles between Unicode and "
"normal mode. In Unicode mode, each group of 4 hexadecimal digits you type "
"becomes a single Unicode character, thus allowing you to enter arbitrary "
"symbols (as long as you know their Unicode codepoints and the font supports "
"them). To finish the Unicode input, press <keycap>Enter</keycap>. For "
"example, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> inserts an em-"
"dash (—). To quit the Unicode mode without inserting anything press "
"<keycap>Esc</keycap>."
msgstr ""
"Saat menggunakan Text tool, menekan <keycap>Ctrl+U</keycap> akan memindahkan "
"anda dari mode normal ke Unicode. Dalam mode Unicode, setiap grup dari 4 "
"digit heksadesmal yang anda ketikkan menjadi sebuah karakter Unicode, yang "
"membuat anda bisa memasukkan simbol arbitrary (selama anda mengetahui poin "
"kode Unicodenya dan fonta yang mendukungnya). Untuk menyelesaikan masukan "
"Unicode, tekan <keycap>Enter</keycap>. Sebagai contoh, <keycap>Ctrl+U 2 0 1 "
"4 Enter<keycap> akan menghasilkan em-dash (—). Untuk keluar dari mode "
"Unicode tanpa memasukkan apapun, tekan <keycap>Esc</keycap>."

#. (itstool) path: sect1/para
#: tutorial-tips.xml:247
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:253
msgid "Using the grid for drawing icons"
msgstr "Menggunakan grid untuk menggambar ikon"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:254
#, fuzzy
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"Misalkan anda ingin membuat sebuah ikon berukuran 24x24 piksel. Buatlah "
"sebuah kanvas berukuran 24x24 (menggunakan <command>Document Preferences</"
"command>) dan atur gridnya menjadi 0.5 px (48x48 gridline). Sekarang, jika "
"anda menata sebuah obyek sejajar dengan gridline - garis grid genap, dan "
"memberikan stroke pada obyek sejajar dengan gridline ganjil yang lebar "
"strokenya adalah sebuah nomor genap, kemudian mengekspornya dalam bawaan "
"90dpi (sehingga 1 px = 1 bitmap pixel), anda akan mendapatkan sebuah gambar "
"bitmap tanpa perlu antialiasing."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:263
msgid "Object rotation"
msgstr "Rotasi obyek"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:264
#, fuzzy
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton role=\"click"
"\">click</mousebutton> again on the object to see the rotation and skew "
"arrows. If the arrows at the corners are clicked and dragged, the object "
"will rotate around the center (shown as a cross mark). If you hold down the "
"<keycap function=\"shift\">Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"Pada Select tool, <keycap>klik</keycap> pada sebuah obyek untuk memunculkan "
"panah skalanya, kemudian <keycap>klil lagi</keycap> pada obyek tersebut "
"untuk melihat panah rotasi dan shiftnya. Jika panah yang ditengah diklik "
"kemudian diseret, titik rotasinya akan berpatokan pada titik tersebut (tanda "
"silang). Jika anda menahan <keycap>Shift</keycap> saat melakukan ini, rotasi "
"akan dilakukan dengan sudut yang berlawanan. Anda bisa menyeret titik rotasi "
"tersebut kemana saja."

#. (itstool) path: sect1/para
#: tutorial-tips.xml:271
#, fuzzy
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The "
"same <keycap>[</keycap> <keycap>]</keycap> keys with <keycap function=\"alt"
"\">Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"Atau, anda juga bisa menggunakan keyboard dengan menekan <keycap>[</keycap> "
"dan <keycap>]</keycap> (per 15 derajat) atau <keycap>Ctrl+[</keycap> dan "
"<keycap>Ctrl+]</keycap> (per 90 derajat). Tombol-tombol <keycap>[]</keycap> "
"jika ditengan menggunakan keycap akan merotasi per pixel."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:280
msgid "Drop shadows"
msgstr "Bayangan"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:281
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:285
#, fuzzy
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"Inkscape mendukung filter SVG Gaussian blur, sehingga anda bisa mudah "
"membuat bayangan untuk sebuah obyek. Pilihlah sebuah obyek, duplikasikan "
"<keycap>Ctrl+D</keycap>, tekan <keycap>PgDown</keycap> untuk meletekkan "
"duplikat tersebut dibawah obyek asal, tempatkan sedikit kekanan dan sedikit "
"lebih rendah dari obyek asal. Berikutnya, buka dialog Fill And Stroke "
"kemudian gantilah nilai Blurnya, misalkan 5.0. Itu saja!"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:294
msgid "Placing text on a path"
msgstr "Meletakkan teks dalam sebuah path"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:295
#, fuzzy
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"Untuk peletakkan teks mengikuti sebuah kurva, pilih teks dan kurvanya, "
"kemudian pilih <command>Put on Path</command> lewat menu Text. Teks tersebut "
"akan mulai dari awal path. Pada umumnya lebih baik membuat sebuah path baru "
"sebagai jalur teks ketimbang menggunakan elemen gambar yang sudah ada - ini "
"akan memberikan anda kontrol yang lebih banyak tanpa merusak gambar."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:303
msgid "Selecting the original"
msgstr "Memilih obyek asal"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:304
#, fuzzy
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"Saat anda memiliki teks dalam path, offset yang berhubungan, atau sebuah "
"klon, obyek asal/sumbernya biasanya sulit untuk diseleksi karna biasanya "
"berada dibawah atau dibuat tidak terlihat dan/atau terkunci. Tombol ajaib "
"<keycap>Shift+D</keycap> bisa membantu anda; pilih teks, offset ataupun "
"klon, kemudian tekan <keycap>Shift+D</keycap> untuk memindahkan seleksi ke "
"path yang berhubungan, sumber offset, atau klon asal."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:314
msgid "Window off-screen recovery"
msgstr "Mengembalikan jendela yang off-screen"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:315
#, fuzzy
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"Saat memindahkan dokumen antara sistem dengan resolusi berbeda atau beberapa "
"tampilan layar, anda mungkin akan menemukan bahwa Inkscape menyimpan posisi "
"jendela ditempat yang tidak bisa ditampilkan oleh layar anda. Cukup maximize "
"jendelanya (yang kemudian akan mengembalikannya ke layar penuh, gunakan task "
"bar), simpan (save), kemudain buka ulang (reload). Anda bisa mencegah "
"kejadian seperti ini dengan melepas tanda centang pada opsi global yang "
"menyimpan geometri jendela (<command>Inkscape Preferences</command> tab "
"<command>Windows</command>)."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:324
msgid "Transparency, gradients, and PostScript export"
msgstr "Transparansi, gradien, dan ekspor PostScrip"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:325
#, fuzzy
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"PostScript atau format EPS tidak mendukung <emphasis>transparansi</"
"emphasis>, sehingga anda sebaiknya tidak menggunakannya jika anda akan "
"mengekspornya kedala PS/EPS. Dalam kasus transparansi sederhana yang hanya "
"memberikan overlay pada satu warna, sangatlah mudah untuk memperbaikinya: "
"Pilih salah satu obyek transparan; berpindahlah ke Dropper tool (<keycap>F7</"
"keycap>); pastikan ia berada dalam mode “pick visible color without alpha”; "
"klik pada obyek yang sama. Itu akan mengambil warna yang tampak kemudian "
"memberikan warna tersebut ke obyek, tapi kali ini, tanpa transparansi. "
"Ulangi untuk semua obyek transparan. Jika obyek transparan anda mengoverlay "
"beberapa area warna, anda harus memecahnya menjadi beberapa potongan dan "
"melakukan prosedur tersebut untuk masing-masing potongan."

#. (itstool) path: sect1/title
#: tutorial-tips.xml:338
msgid "Interactivity"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:339
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:343
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:349
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap function=\"shift\">Shift</keycap><keycap>O</keycap></"
"keycombo>). Here you can implement arbitrary functionality using JavaScript. "
"Some basic examples:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:356
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:361
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:368
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:373
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape.org"
"\",\"_blank\");</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:380
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:385
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:390
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. (itstool) path: Work/format
#: tips-f01.svg:49 tips-f02.svg:49 tips-f03.svg:49 tips-f04.svg:70
#: tips-f05.svg:558 tips-f06.svg:136
msgid "image/svg+xml"
msgstr "image/svg+xml"

#, fuzzy
#~ msgid "Click with the right mouse button"
#~ msgstr "Klik ganda dengan tombol kiri tetikus"

#, fuzzy
#~ msgid "Select the Pen tool from the toolbar"
#~ msgstr "Memilih Pen tool lagi"

#~ msgid ""
#~ "Exporting <emphasis>gradients</emphasis> to PS or EPS does not work for "
#~ "text (unless text is converted to path) or for stroke paint. Also, since "
#~ "transparency is lost on PS or EPS export, you can't use e.g. a gradient "
#~ "from an <emphasis>opaque</emphasis> blue to <emphasis>transparent</"
#~ "emphasis> blue; as a workaround, replace it by a gradient from "
#~ "<emphasis>opaque</emphasis> blue to <emphasis>opaque</emphasis> "
#~ "background color."
#~ msgstr ""
#~ "Mengekspor <emphasis>gradien</emphasis> menjadi PS atau EPS tidaklah bisa "
#~ "dilakukan untuk teks (kecuali teks sudah dikonversi menjadi path) atau "
#~ "untuk stroke paint. Juga, dikarenakan transparansi hilang saat ekspor ke "
#~ "PS atau EPS dilakukan, anda tidak bisa menggunakan, misalnya, gradiasi "
#~ "dari biru <emphasis>jelas</emphasis> ke biru <emphasis>transparan</"
#~ "emphasis>; gantilah dengan gradiasi dari biru <emphasis>jelas</emphasis> "
#~ "ke biru <emphasis>jelas</emphasis> juga, hanya saja, biru yang kedua "
#~ "adalah warna latar belakang."

#~ msgid "Clipping or masking a bitmap"
#~ msgstr "Clipping atau masking sebuah bitmap"

#~ msgid ""
#~ "By default, an imported bitmap (e.g. a photo) is an image element which "
#~ "is not editable by the Node tool. To work around this, convert the image "
#~ "into a rectangle with pattern fill by <command>Object to Pattern</"
#~ "command> (<keycap>Alt+I</keycap>). This will give you a rectangle "
#~ "<emphasis>filled</emphasis> with your bitmap. Now this object can be "
#~ "converted to path, node-edited, intersected with other shapes etc. In "
#~ "<command>Inkscape Preferences</command> (<command>Misc</command> tab), "
#~ "you can set the option of always importing bitmaps as pattern-filled "
#~ "rectangles."
#~ msgstr ""
#~ "Bawaanya, bitmap yang diimpor (misalnya foto) adalah elemen gambar yang "
#~ "tidak bisa diedit dengan Node tool. Untuk bisa mengeditnya, konversikan "
#~ "gambar tersebut menjadi sebuah rectange dengan isi pattern lewat "
#~ "<command>Object to pattern</command> (<keycap>Alt+I</keycap>. Ini akan "
#~ "menghasilkan sebuah rectangle yang <emphasis>berisi</emphasis> bitmap/"
#~ "gambar anda. Sekarang obyek ini akan bisa dikonversikan ke path, diedit "
#~ "nodenya, dipotong dengan bentuk lain, dsbg. Pada <command>Inkscape "
#~ "Preferences</command> (tab <command>Misc</command>, anda bisa menata "
#~ "pilihan apakah sebuah bitmap akan selalu diimpor menjadi rectangle yang "
#~ "berisi bitmap tersebut atau tidak."

#~ msgid "Open dialog as an object palette"
#~ msgstr "Membuka dialog seperti sebagai palet obyek"

#~ msgid ""
#~ "If you have a number of small SVG files whose contents you often reuse in "
#~ "other documents, you can conveniently use the Open dialog as a palette. "
#~ "Add the directory with your SVG sources into the bookmarks list so you "
#~ "can open it quickly. Then browse that directory looking at the previews. "
#~ "Once you found the file you need, simply drag it to the canvas and it "
#~ "will be imported into your current document."
#~ msgstr ""
#~ "Jika anda memiliki beberapa berkas SVG kecil yang kontennya sering anda "
#~ "gunakan lagi di dokumen lain, anda bisa menggunakan Open dialog as a "
#~ "pallete. Tambahkan direktori yang berisi sumber SVG kedalam daftar "
#~ "bookmarks sehingga anda bisa cepat membukanya. Kemudian telusuri "
#~ "direktorinya untuk melihat pratilik. Setelah anda menemukan berkasnya, "
#~ "drag saja ke kanvas dan itu akan langsung diimpor kedalam dokumen."
